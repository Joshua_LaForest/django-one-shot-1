## Feature 7
 * [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "My Lists"
* [x] a table that has two columns:
* [x] the first with the header "Name" and the rows with the names of the Todo lists
* [x] the second with the header "Number of items" and nothing in those rows because we don't yet have tasks


## feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [ ] Create a template to show the details of the todolist and a table of its to-do items
* [x] Update the list template to show the number of to-do items for a to-do list
* [x] Update the list template to have a link from the to-do list name to the detail view for that to-do list

* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the to-do list's name as its content
* [x] an h2 tag that has the content "Tasks"
* [x] a table that contains two columns with the headers "Task" and "Due date" with rows for each task in the to-do list
* [x] The to-do list view should now contain an a tag to the to-do list detail page for the indicated to-do list in the table's first column.


## Feature 9
* [x] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list
* [x] Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
* [x] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
* [x] Add a link to the list view for the TodoList that navigates to the new create view
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "Create a new list"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include:
* [x] an input tag with type "text" and name "name"
* [x] a button with the content "Create"
* [x] Create an update view for the TodoList model that will show the name. field in the form and handle the form submission * [ ] to change an existing TodoList
If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
* [x] Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "Update list"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include:
* [x] an input tag with type "text" and name "name"
* [x] a button with the content "Update"

## feature 10
* [x] Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList
* [x] If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
* [x] Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "Update list"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include:
* [x] an input tag with type "text" and name "name"
* [x] a button with the content "Update"

## feature 11
* [x] Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
* [x] If the to-do list is successfully deleted, it should redirect to the to-do list list view.
* [x] Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
* [x] Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
* [x] Add a link to the detail view for the TodoList that navigates to the new delete view.
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] a h1 tag that contains the text, "Are you sure?"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include:
* [x] a button with the content "Delete"
* [x] Update the todo detail view
* [x] a p tag that contains:
* [x] and a tag with an href attribute that points to the create todo list URL and has content "Delete"

## feature 12
* [x] Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".
* [x] Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).
* [ ] Add a link to the list view for the TodoList that navigates to the new create view.
* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "Create a new item"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include:
* [x] an input tag with type "text" and name "task"
* [x] an input tag with type "text" and name "due date"
* [x] an input tag with type "checkbox" and name "is_completed"
* [ ] a select list with the name "list"
* [x] a button with the content "Create"
* [x] Update the todo list view
* [x] a p tag that contains
* [x] an a tag with an href attribute that points to the create to-do item URL and has content "Create a new item"



