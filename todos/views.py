from ast import List
from django.shortcuts import get_object_or_404, render, redirect
from .forms import New_list, TodoItemForm
from .models import Todo_item, Todo_list


def show_lists(request):
    todo_lists = Todo_list.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)


def list_details(request, pk):
    todo_list = Todo_list.objects.get(pk=pk)
    context = {"todo_list": todo_list}
    return render(request, "todos/details.html", context)


def create_list(request):
    context = {}
    form = New_list(request.POST or None)
    if form.is_valid():

        todo_lists = form.save()
        return redirect("list_details", todo_lists.pk)
    context["form"] = form
    return render(request, "todos/create.html", context)

# def create_list(request):
#     if request.method == "POST" or None:
#         form = New_list(request.POST)
#         if form.is_valid():
#             todo_list = form.save()
#             return redirect("list_details", pk=todo_list.pk)
#     elif New_list:
#         form = New_list()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "todos/create.html", context)

def update_list(request, pk):
    context = {}
    obj = get_object_or_404(Todo_list, pk = pk)
    form  = New_list(request.POST or None, instance= obj)
    if form.is_valid():
        todo_list = form.save()
        return redirect("list_details", todo_list.pk)
    context['form']= form
    return render(request, "todos/update.html", context)

def delete(request, pk):
    context = {}
    todo_list = get_object_or_404(Todo_list, pk =pk)
    if request.method == "POST":
        todo_list.delete()
        return redirect("show_lists")
    context['todo_list'] = todo_list
    return render(request, "todos/delete.html", context)


def create_task_item(request):
    context = {}
    if request.method == "POST":
        form = TodoItemForm(request.POST or None)
        if form.is_valid():
            todo_item = form.save()
            return redirect("list_details", todo_item.list.pk)
    
        else:
            # form = TodoItemForm()
            context["form"] = form
    return render(request, "todo_items/create.html", context)


def update_todo_item(request, pk):
    todoitem = get_object_or_404(Todo_item, pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("list_details", todoitem.list.pk)
    else:
        form = TodoItemForm(instance=todoitem)
    return render(request, "todo_items/update.html", {"form": form})