from tkinter import CASCADE
from django.db import models

# Create your models here.
class Todo_list(models.Model):
    name = models.CharField(max_length=100, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.name


class Todo_item(models.Model):
    task = models.CharField(max_length=100, blank=True)
    due_date = models.DateTimeField(blank=True, null=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        Todo_list, related_name="items", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.task
