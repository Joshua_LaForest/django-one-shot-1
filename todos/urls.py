from django.urls import path
from django.contrib import admin
from .views import delete, list_details, show_lists, create_list, update_list, delete, create_task_item, update_todo_item


urlpatterns = [
    path("", show_lists, name="show_lists"),
    path("<int:pk>/", list_details, name="list_details"),
    path("create/", create_list, name="create_list"),
    path("<int:pk>/update/", update_list, name = "update_list"),
    path("<int:pk>/delete", delete, name = "delete"),
    path("items/create/", create_task_item, name = "create_task_item"),
    path("items/<int:pk>/edit", update_todo_item, name = "update_todo_item"),
]
