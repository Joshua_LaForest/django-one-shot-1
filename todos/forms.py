# from unicodedata import name
from django import forms
from .models import Todo_list, Todo_item
from django.forms import ModelForm

class New_list(forms.ModelForm):
    class Meta:
        model = Todo_list
        fields = [
            "name",
        ]

class TodoItemForm(ModelForm):
    class Meta:
        model = Todo_item
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
            ]
